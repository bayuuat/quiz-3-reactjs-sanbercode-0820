import React, { Component } from 'react';
import axios from 'axios';
import './home.css';

class Movies extends Component {
    constructor(props) {
        super(props);
        this.state = {
            daftarMovie: [],
            title: '',
            description: '',
            year: '',
            duration: '',
            genre: '',
            rating: '',
            image_url: '',
        };
    }

    componentDidMount() {
        axios
            .get(`http://backendexample.sanbercloud.com/api/movies`)
            .then((res) => {
                const movie = res.data;
                this.setState({
                    daftarMovie: movie,
                });
            });
    }

    render() {
        console.log(this.state.daftarMovie);
        return (
            <div className="container">
                <h1>Daftar Film Terbaik</h1>
                {this.state.daftarMovie.map((val, index) => {
                    return (
                        <div key={index} className="movie-list">
                            <div className="top">
                                <img src={val.image_url} alt="poster-film" />
                                <div className="ket">
                                    <h2>{val.title}</h2>
                                    <p>Durasi : {val.duration} menit </p>
                                    <p>Genre : {val.genre} </p>
                                    <p>Rating : {val.rating} </p>
                                </div>
                            </div>
                            <div className="bot">
                                <p>Deskripsi : {val.description}</p>
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default Movies;

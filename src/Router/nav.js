import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../Public/img/logo.png';
import './nav.css';

const Nav = () => {
    return (
        <header className="Navbar">
            <img src={Logo} style={{ width: 200 }} alt="logo-sanber" />
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li>
                        <Link to="/movielist">Movie List Editor</Link>
                    </li>
                    <li>
                        <Link to="/login">Login</Link>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Nav;

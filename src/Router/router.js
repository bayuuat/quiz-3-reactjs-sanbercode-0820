import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../Home Page/home';
import About from '../About Page/about';
import Movies from '../Moive List Editor/Movies';
import Login from '../Login/login';
import Nav from './nav';

const App = () => {
    return (
        <>
            <Nav />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/about" component={About} />
                <Route exact path="/movielist" component={Movies} />
                <Route exact path="/login" component={Login} />
            </Switch>
        </>
    );
};

export default App;

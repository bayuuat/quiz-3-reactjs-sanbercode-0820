import React, { useContext } from 'react';
import { MoviesContext } from './MoviesContext';
import axios from 'axios';

const MovieForm = () => {
    const [movies, setMovies, input, setInput] = useContext(MoviesContext);

    const handleChange = (event) => {
        let typeOfInput = event.target.name;

        switch (typeOfInput) {
            case 'title': {
                setInput({ ...input, title: event.target.value });
                break;
            }
            case 'description': {
                setInput({ ...input, description: event.target.value });
                break;
            }
            case 'year': {
                setInput({ ...input, year: event.target.value });
                break;
            }
            case 'duration': {
                setInput({ ...input, duration: event.target.value });
                break;
            }
            case 'genre': {
                setInput({ ...input, genre: event.target.value });
                break;
            }
            case 'rating': {
                setInput({ ...input, rating: event.target.value });
                break;
            }
            case 'image_url': {
                setInput({ ...input, image_url: event.target.value });
                break;
            }
            default: {
                break;
            }
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (input.id === null) {
            axios
                .post(`http://backendexample.sanbercloud.com/api/movies`, {
                    title: input.title,
                    description: input.description,
                    year: input.year,
                    duration: input.duration,
                    genre: input.genre,
                    rating: input.rating,
                    image_url: input.image_url,
                })
                .then((res) => {
                    setMovies([
                        ...movies,
                        {
                            id: res.data.id,
                            title: input.title,
                            description: input.description,
                            year: input.year,
                            duration: input.duration,
                            genre: input.genre,
                            rating: input.rating,
                            image_url: input.image_url,
                        },
                    ]);
                });
        } else {
            axios
                .put(
                    `http://backendexample.sanbercloud.com/api/movies/${input.id}`,
                    {
                        title: input.title,
                        description: input.description,
                        year: input.year,
                        duration: input.duration,
                        genre: input.genre,
                        rating: input.rating,
                        image_url: input.image_url,
                    }
                )
                .then(() => {
                    let newDataMovies = movies.map((x) => {
                        if (x.id === input.id) {
                            x.title = input.title;
                            x.description = input.description;
                            x.year = input.year;
                            x.duration = input.duration;
                            x.genre = input.genre;
                            x.rating = input.rating;
                            x.image_url = input.image_url;
                        }
                        return x;
                    });
                    setMovies(newDataMovies);
                    setInput({
                        title: '',
                        description: '',
                        year: 2020,
                        duration: 120,
                        genre: '',
                        rating: 0,
                        image_url: '',
                        id: null,
                    });
                });
        }
    };

    return (
        <div className="container">
            <h1>Movies Form</h1>
            <div style={{ width: '70%', margin: '0 auto', display: 'block' }}>
                <div style={{ border: '1px solid #aaa', padding: '20px' }}>
                    <form onSubmit={handleSubmit}>
                        <div className="label-form">
                            <label style={{ float: 'left' }}>
                                <b>Title:</b>
                            </label>
                            <input
                                style={{ float: 'right', width: 220 }}
                                type="text"
                                required
                                name="title"
                                value={input.title}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="label-form">
                            <label style={{ float: 'left' }}>
                                <b>Description:</b>
                            </label>
                            <textarea
                                style={{
                                    float: 'right',
                                    width: 400,
                                    height: 80,
                                }}
                                type="text"
                                required
                                name="description"
                                value={input.description}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="label-form">
                            <label style={{ float: 'left' }}>
                                <b>Year:</b>
                            </label>
                            <input
                                min={1980}
                                style={{ float: 'right', width: 100 }}
                                type="number"
                                required
                                name="year"
                                value={input.year}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="label-form">
                            <label style={{ float: 'left' }}>
                                <b>Duration:</b>
                            </label>
                            <input
                                style={{ float: 'right' }}
                                type="number"
                                required
                                name="duration"
                                value={input.duration}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="label-form">
                            <label style={{ float: 'left' }}>
                                <b>Genre:</b>
                            </label>
                            <input
                                style={{ float: 'right' }}
                                type="text"
                                required
                                name="genre"
                                value={input.genre}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="label-form">
                            <label style={{ float: 'left' }}>
                                <b>Rating:</b>
                            </label>
                            <input
                                min={0}
                                max={10}
                                style={{ float: 'right', width: 60 }}
                                type="number"
                                required
                                name="rating"
                                value={input.rating}
                                onChange={handleChange}
                            />
                        </div>
                        <div className="label-form">
                            <label style={{ float: 'left' }}>
                                <b>Image Url:</b>
                            </label>
                            <textarea
                                style={{
                                    float: 'right',
                                    width: 300,
                                    height: 60,
                                }}
                                type="text"
                                required
                                name="image_url"
                                value={input.image_url}
                                onChange={handleChange}
                            />
                        </div>
                        <div style={{ width: '100%', paddingBottom: '20px' }}>
                            <button style={{ float: 'right' }}>submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default MovieForm;

import React, { useContext, useState, useEffect } from 'react';
import { MoviesContext } from './MoviesContext';
import axios from 'axios';

const MovieList = () => {
    const [movies, setMovies, input, setInput] = useContext(MoviesContext);
    const [search, setSearch] = useState('');
    const [filteredMovies, setFilteredMovies] = useState(null);

    const handleDelete = (event) => {
        let idMovies = parseInt(event.target.value);

        let newMovies = movies.filter((el) => el.id !== idMovies);

        axios
            .delete(
                `http://backendexample.sanbercloud.com/api/movies/${idMovies}`
            )
            .then((res) => {
                console.log(res);
            });

        setMovies([...newMovies]);
    };

    const handleEdit = (event) => {
        let idMovies = parseInt(event.target.value);
        let newMovies = movies.find((x) => x.id === idMovies);
        setInput({
            title: newMovies.title,
            description: newMovies.description,
            year: newMovies.year,
            duration: newMovies.duration,
            genre: newMovies.genre,
            rating: newMovies.rating,
            image_url: newMovies.image_url,
            id: newMovies.id,
        });
    };

    useEffect(() => {
        if (movies !== null) {
            setFilteredMovies(
                movies.filter((item) => {
                    return item.title
                        .toLowerCase()
                        .includes(search.toLocaleLowerCase());
                })
            );
        }
    }, [search, movies]);

    const updateSearch = (event) => {
        setSearch(event.target.value.substr(0, 20));
    };

    return (
        <div className="container">
            <h1>Daftar Harga Buah</h1>
            <div style={{ width: '90%' }}>
                <input
                    type="text"
                    placeholder="Search"
                    style={{ float: 'right' }}
                    value={search}
                    onChange={updateSearch}
                />
            </div>

            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Year</th>
                        <th>Duration</th>
                        <th>Genre</th>
                        <th>Rating</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredMovies !== null &&
                        filteredMovies.map((item, index) => {
                            return (
                                <tr key={item.id}>
                                    <td>{index + 1}</td>
                                    <td>{item.title}</td>
                                    <td>
                                        <p className="desc">
                                            {item.description}
                                        </p>
                                    </td>
                                    <td>{item.year}</td>
                                    <td>{item.duration}</td>
                                    <td>{item.genre}</td>
                                    <td>{item.rating}</td>
                                    <td>
                                        <button
                                            onClick={handleEdit}
                                            value={item.id}
                                        >
                                            Edit
                                        </button>
                                        &nbsp;
                                        <button
                                            onClick={handleDelete}
                                            value={item.id}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
};

export default MovieList;

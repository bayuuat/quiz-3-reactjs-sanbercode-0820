import React from 'react';
import { MoviesProvider } from './MoviesContext';
import MovieList from './ListMovie';
import MovieForm from './MoviesForm';
import './moviesedit.css';

const Movie = () => {
    return (
        <>
            <MoviesProvider>
                <MovieList />
                <MovieForm />
            </MoviesProvider>
        </>
    );
};

export default Movie;

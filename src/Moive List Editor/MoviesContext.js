import React, { useState, useEffect, createContext } from 'react';
import axios from 'axios';

export const MoviesContext = createContext();

export const MoviesProvider = (props) => {
    const [movies, setMovies] = useState(null);
    const [input, setInput] = useState({
        title: '',
        description: '',
        year: 2020,
        duration: 120,
        genre: '',
        rating: 0,
        image_url: '',
        id: null,
    });

    useEffect(() => {
        if (movies === null) {
            axios
                .get(`http://backendexample.sanbercloud.com/api/movies`)
                .then((res) => {
                    setMovies(
                        res.data.map((el) => {
                            return {
                                id: el.id,
                                title: el.title,
                                description: el.description,
                                year: el.year,
                                duration: el.duration,
                                genre: el.genre,
                                rating: el.rating,
                                image_url: el.image_url,
                            };
                        })
                    );
                });
        }
    }, [movies]);

    return (
        <MoviesContext.Provider value={[movies, setMovies, input, setInput]}>
            {props.children}
        </MoviesContext.Provider>
    );
};

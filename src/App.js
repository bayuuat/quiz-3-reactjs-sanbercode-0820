import React from 'react';
import './App.css';
import FinalApp from './Router/router';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {
    return (
        <Router>
            <FinalApp />
        </Router>
    );
}

export default App;

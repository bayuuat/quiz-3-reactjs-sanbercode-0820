import React from 'react';

const About = () => {
    return (
        <div className="container">
            <div>
                <h1>Data Peserta Sanbercode Bootcamp ReactJS</h1>
                <p>
                    <b>1. Nama : </b> Bayu Aditya Triwibowo
                </p>
                <p>
                    <b>2. Email : </b> bayuuat@gmail.com
                </p>
                <p>
                    <b>3. Sistem Operasi yang digunakan : </b> windows 64 bit
                </p>
                <p>
                    <b>4. Akun Gitlab : </b> @bayuuat
                </p>
                <p>
                    <b>5. Akun Telegram : </b> @bayuuat
                </p>
            </div>
        </div>
    );
};

export default About;
